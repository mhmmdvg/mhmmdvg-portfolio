import BelangImage from '../../public/project/belangproject.png';
import ProjectOne from '../../public/project/project1.png';
import ProjectTwo from '../../public/project/project2.png';

const ListProject = [
  {
    title: 'Belang.kom',
    image: BelangImage,
    tag: ['e-learning', 'android studio', 'xml'],
    desc: 'E-learning android application using Java Android Studio.',
  },
  {
    title: 'Icon IBOX',
    image: ProjectOne,
    tag: ['android', 'ios', 'flutter', 'dart'],
    desc: 'Icon IBOX is a mobile application that provides a platform for users to store their item.',
  },
  {
    title: 'My First Site',
    image: ProjectTwo,
    tag: ['html', 'css', 'javascript'],
    desc: 'My first site using HTML, CSS, and Javascript.',
  },
];

export default ListProject;
